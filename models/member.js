let connection = require('../config/db')
let moment = require('../config/moment')

class Member{

    constructor(row){
        this.row = row
    }

    get id(){
        return this.row.id
    }

    get login(){
        return this.row.login
    }

    get password(){
        return this.row.password
    }

    get email(){
        return this.row.email
    }

    get nom(){
        return this.row.nom
    }

    get nom(){
        return this.row.prenom
    }

    get created_at(){
        return moment(this.row.created_at)
    }

    static register(member, callback){
        connection.query("INSERT INTO member SET login = ?, password = ?, nom = ?, prenom =?, email =?, created_at = ?",
            [member.login, member.password, member.nom, member.prenom, member.email, new Date()],
            (err, result) =>{
                if (err) throw err
                callback(result)
            }
        )
    }

    static all(callback){
        connection.query("SELECT * FROM member;", (err, rows) =>{
            if (err) throw err
            callback(rows.map((row) => new Member(row)))
        })
    }

    static find(member, callback){
        connection.query("SELECT * FROM member WHERE id = ? OR login = ? OR email = ? LIMIT 1;", [member.id, member.login, member.email], (err, rows) =>{
            if (err) throw err
            callback(new Member(rows[0]))
        })
    }

    static login(body, callback){
        connection.query("SELECT * FROM member WHERE login =? AND password = ? LIMIT 1;", [body.login, body.password], (err, rows) =>{
            if (err) throw err
            callback(new Member(rows[0]))
        })
    }
}

module.exports = Member
