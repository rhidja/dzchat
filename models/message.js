let connection = require('../config/db')
let moment = require('../config/moment')

class Message{

    constructor(row){
        this.row = row
    }

    get id(){
        return this.row.id
    }

    get content(){
        return this.row.content
    }

    get author(){
        return this.row.author
    }

    get created_at(){
        return moment(this.row.created_at)
    }

    static find(id, callback){

    }

    static create(message, callback){
        connection.query("INSERT INTO message SET content = ?, author = ?, created_at = ?", [message.content, message.author, message.created_at], (err, result) =>{
            if (err) throw err
            callback(result)
        })
    }

    static all(callback){
        connection.query("SELECT * FROM message;", (err, rows) =>{
            if (err) throw err
            callback(rows.map((row) => new Message(row)))
        })
    }

    static find(id, callback){
        connection.query("SELECT * FROM message WHERE id =? LIMIT 1;", [id], (err, rows) =>{
            if (err) throw err
            callback(new Message(rows[0]))
        })
    }
}

module.exports = Message
