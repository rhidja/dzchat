(function ($) {
    $("body").animate({ scrollTop : $("body").prop('scrollHeight')}, 500);
    
    // Connexion à socket.io
    $(".fa-circle").css('color','grey')
    var socket = io.connect('http://localhost:3000');

    socket.on('connect', ()=>{
        // at connection, first send my username
        socket.emit('member', {login : $("#member-login").text(), id : $("#member-id").text()})
    })

    socket.on('logged', (member)=>{
        $("#member-"+member.id).css('color','green')
        $("#listMessages").append('<tr><td class="text-primary text-center" colspan="3">'+member.login+' has joined the chat at '+member.join_at+'</td></tr>')
    })

    socket.on('loggedout', (member)=>{
        $("#member-"+member.id).css('color','gray')
        $("#listMessages").append('<tr><td class="text-muted text-center" colspan="3">'+member.login+' has left the chat at '+member.left_at+'</td></tr>')
    })

    $("#message-from").submit(function(event){
        event.preventDefault();
        let message = $("#message").val()
        $("#message").val('')
        socket.emit("newMessage", message)
    });

    socket.on("addMessage", (message) =>{
        $("#listMessages").append('<tr><td class="text-primary">'+message.author+'</td><td>'+ message.content +'</td><td>'+ message.created_at + '</td></tr>')
        $("body").animate({ scrollTop : $("body").prop('scrollHeight')}, 500);
    })

})(jQuery);
