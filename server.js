var express = require('express')
var app = express()
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;

var bodyParser = require('body-parser')
var session = require('express-session')

// Moteur de templates.
app.set('view engine', 'ejs')

// Middlewares.
app.use('/assets', express.static('public')) // Déclaration des static
app.use(bodyParser.urlencoded({ extended: false })) // parse application/x-www-form-urlencoded
app.use(bodyParser.json()) // parse application/json
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
}))
app.use(require('./middlewares/flash'))

// Routes.
app.get('/chat', (request, response, next) =>{
    if (request.session.member) {
        //console.log(request.session.member)
        let Member = require('./models/member')
        Member.all((members) =>{
            let Message = require('./models/message')
            Message.all((messages) => {

                response.render("pages/index",{member : request.session.member ,  members : members, messages : messages})
            })
        })
    } else {
        // Otherwise we redirect him to login form
        response.redirect("/");
    }
})

app.get('/', (request, response) =>{
    if (request.session.member) {
        response.redirect("/chat");
    } else {
        response.render("pages/login")
    }
})

app.post('/', (request, response) =>{
    if(request.body.login === undefined || request.body.login === '' || request.body.password === undefined || request.body.password === ''){
        request.flash("error","Login ou mot de passe vide")
        response.redirect('/')
    }else {
        let Member = require('./models/member')
        Member.login(request.body, (member) => {
            if(member.row === undefined ){
                request.flash("error","Login ou mot de passe incorrect")
                response.redirect('/')
            }else {
                request.session.member = member.row;
                response.redirect('/chat')
            }
        })
    }
})

app.get('/logout', (request, response) =>{
    if (request.session.member) {
        request.session.member = null
    }
    response.redirect("/");
})

app.post('/register', (request, response) =>{
    let body = request.body
    if(body.login === '' || body.password === '' || body.email === ''){
        request.flash("error","L'un des champs est vide")
        response.redirect('/register')
    }else {
        let Member = require('./models/member')
        Member.register(body, (member) => {
            request.flash("success","Vous êtes bien inscrit")
            response.redirect('/')
        })
    }
})

app.get('/message/:id',(request, response) => {
    let Message =require('./models/message')
    Message.find(request.params.id, (message) => {
        response.render("messages/show",{message : message})
    })
})

var members = {}
var nombres = 0
io.on('connection', (socket) => {
    let moment = require('./config/moment')
    var me;

    for(var k in members){
        socket.emit('logged',members[k])
    }

    socket.on('member', (member) => {
        me = member;
        members[me.id] = me
        me.join_at = moment(new Date()).fromNow()
        nombres++
        io.sockets.emit('logged',me)
    })

    socket.on('disconnect',() => {
        delete members[me.id]
        me.left_at = moment(new Date()).fromNow()
        nombres--
        io.sockets.emit('loggedout', me)
    })

    socket.on("newMessage", (content) => {
        if(content === undefined || content === ''){
            // Logic
        }else {
            let Message = require('./models/message')
            let message = { content : content, author : me.login, created_at : new Date() }
            Message.create(message, () => {
                message.created_at = moment(new Date()).fromNow()
                io.sockets.emit("addMessage", message)
            })
        }
    })
})

server.listen(port, () => {
    console.log('Server listening at port %d', port);
});
